FROM python:3.7-alpine
RUN addgroup -S zhuimagroup && adduser -S zhuima -G zhuimagroup
#creates work dir
WORKDIR /app
#copy python script to the container folder app

COPY /python-hello-zhuima.py /app/python-hello-zhuima.py
RUN chmod +x /app/python-hello-zhuima.py
ENTRYPOINT ["python", "/app/python-hello-zhuima.py"]